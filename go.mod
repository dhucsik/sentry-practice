module practice

go 1.20

require github.com/getsentry/sentry-go v0.24.0

require (
	golang.org/x/sys v0.6.0 // indirect
	golang.org/x/text v0.8.0 // indirect
)
