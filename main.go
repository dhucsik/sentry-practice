package main

import (
	"github.com/getsentry/sentry-go"
	"log"
	"net/http"
	"time"
)

func main() {
	err := sentry.Init(sentry.ClientOptions{
		Dsn: "https://7e750c14b384da2b4e65cfb82f6f0dc1@o4505865694609408.ingest.sentry.io/4505865696509952",

		EnableTracing: true,
		// Specify a fixed sample rate:
		// We recommend adjusting this value in production
		TracesSampleRate: 1.0,
		// Or provide a custom sample rate:
		TracesSampler: sentry.TracesSampler(func(ctx sentry.SamplingContext) float64 {
			// As an example, this does not send some
			// transactions to Sentry based on their name.
			if ctx.Span.Name == "GET /health" {
				return 0.0
			}

			return 1.0
		}),
	})
	if err != nil {
		log.Fatal(err)
	}

	http.HandleFunc("/ran", func(writer http.ResponseWriter, request *http.Request) {
		ctx := request.Context()
		sentryTrace := request.Header.Get("sentry-trace")

		transaction := sentry.StartSpan(ctx, "handler ran", sentry.ContinueFromTrace(sentryTrace), sentry.WithTransactionName("namee"))

		func() { // suppose it is some function in another package like repository
			span := sentry.StartSpan(transaction.Context(), "database call", sentry.WithTransactionName("database span"))
			time.Sleep(3 * time.Second)
			span.Finish()
		}()

		func() {
			span := sentry.StartSpan(transaction.Context(), "some another operation", sentry.WithTransactionName("another span"))
			time.Sleep(3 * time.Second)
			span.Finish()
		}()

		transaction.Finish()
	})

	http.HandleFunc("/ss", func(writer http.ResponseWriter, request *http.Request) {
		ctx := request.Context()
		transaction := sentry.StartSpan(ctx, "handler main", sentry.WithTransactionName("handlerrr"))

		req, _ := http.NewRequestWithContext(transaction.Context(), http.MethodGet, "http://localhost:8080/ran", nil)
		req.Header.Set("sentry-trace", transaction.ToSentryTrace())

		_, err := http.DefaultClient.Do(req)
		if err != nil {
			log.Println(err.Error())
		}

		span := sentry.StartSpan(transaction.Context(), "some operation", sentry.WithTransactionName("some operation"))
		time.Sleep(5 * time.Second)
		span.Finish()

		transaction.Finish()
	})

	if err := http.ListenAndServe("localhost:8080", nil); err != nil {
		log.Fatal(err)
	}
}
